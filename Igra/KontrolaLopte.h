#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <math.h>
# define M_PI           3.14159265358979323846f
#include <iostream>
using namespace std;
class KontrolaLopte
{
	sf::IntRect textureSize;
	float pomjerajTeksture = 0.f;//ne dirati
	float brzina = 100.f;//piksela po sekundi
public:
	sf::CircleShape lopta;
	const float radius = 15.f;
	float ugao = -170.f;//u stepenima
	bool odbijeno = false;


	KontrolaLopte(float x, float y, float _brzina=100.f) //konstruktor kontrole lopte
	{
		brzina = _brzina;
		//kreiranje lopte
		lopta=sf::CircleShape(radius);
		lopta.setOrigin(radius, radius);
		lopta.setPosition(x, y);
		//podesavanje teksture
		sf::Texture* a = new sf::Texture();
		a->loadFromFile("Resources\\ball.png");
		a->setRepeated(true);
		lopta.setTexture(a);
		//pravougaonik koji obuhvata trenutni dio teksture
		textureSize.top = 0;
		textureSize.left = 0;
		textureSize.height = a->getSize().x;
		textureSize.width = a->getSize().y;
	}
	void crtaj(sf::RenderWindow &window, float deltaTime) {
		sf::Vector2u velicinaProzora = window.getSize();
		lopta.setRotation(ugao - 90);
		//provjera da li se desilo sudaranje u poslednjem ciklusu te pomjera loptu ako nije
		if (!odbijeno) {
			lopta.move(cos(lopta.getRotation()*M_PI / 180.0f) * brzina*deltaTime,
				sin(lopta.getRotation()*M_PI / 180.0f)*brzina*deltaTime);
		}
		else {//ako jeste kaze da nije
			odbijeno = false;
		}

		//provjera da li lopta izlazi van prozora
		if (lopta.getPosition().x - radius < 0.f)//lijevo
		{
			ugao = -ugao;
			lopta.setPosition(radius + 0.1f, lopta.getPosition().y );
		}
		if (lopta.getPosition().x + radius > velicinaProzora.x)//desno
		{
			ugao = -ugao;
			lopta.setPosition(velicinaProzora.x - radius - 0.1f, lopta.getPosition().y);
		}
		if (lopta.getPosition().y - radius < 0.f)//gore
		{
			ugao = 180-ugao;
			lopta.setPosition(lopta.getPosition().x, radius + 0.1f);
		}
		
		//pomjeranje teksture lopte
		lopta.setRotation(ugao - 90);
		pomjerajTeksture = (pomjerajTeksture + brzina*deltaTime);
		textureSize.left = int(pomjerajTeksture);
		lopta.setTextureRect(textureSize);
		
		//crtanje
		window.draw(lopta);
	}

	void odbiSe(sf::Vector2f centar, sf::Vector2f velicina) {//odbijanje s kockicama
		odbijeno = true;
		float x = (centar.x - lopta.getPosition().x)*velicina.y;
		float y = (centar.y - lopta.getPosition().y)*velicina.x;
		if (abs(y) >= abs(x)) {
			ugao = 180-ugao; //gore/dole
			lopta.move(0, (y <= 0) ? 0.1f : -0.1f);
		}
		else {
			ugao = - ugao; //lijevo/desno
			lopta.move((x <= 0) ? 0.1f : -0.1f, 0);
		}
	}

	~KontrolaLopte() //destruktor
	{
		delete lopta.getTexture(); //brisanje objekta teksture
	}
};
