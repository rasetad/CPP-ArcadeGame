#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include "NizBoje.h"
using namespace std;

class KontrolaNivoa
{
	sf::RectangleShape **elementi;
	int brojElemenata=0;
	NizBoje boje;
	int visina;
	int sirina;
	bool izgubio = false;

	KontrolaLopte *lopta;
	sf::RectangleShape udaraljka;
	sf::Vector2f velicinaEkrana;
public:

	KontrolaNivoa(string filename, const sf::Vector2f _velicinaEkrana)
	{
		velicinaEkrana = _velicinaEkrana;
		ifstream file(filename);
		if (file.is_open()) {
			//unosenje podataka
			float visinaPixela;
			int x, r, g, b;
			float brzinaLopte;
			file >> brzinaLopte;
			float sirinaUdaraljke;
			file >> sirinaUdaraljke;
			file >> r >> g >> b;
			sf::Color bojaUdaraljke = sf::Color::Color(r, g, b);
			file >> visinaPixela;
			file >> sirina;
			file >> visina;
			vector<string> kodovi(visina);
			for (int i = 0; i < visina; i++) {
				file >> kodovi[i];
			}
			boje.promjeniDuzinu(10);
			//unosenje boja
			while (!file.eof()) {
				file >> x >> r >> g >> b;
				boje[x].r = r;
				boje[x].g = g;
				boje[x].b = b;
			}
			file.close();
			//pravljenje kockica
			elementi = new  sf::RectangleShape*[visina*sirina];
			float sirinaEl=velicinaEkrana.x/sirina, visinaEl = visinaPixela/visina;
			for (int i = 0; i < visina; i++) {
				for (int j = 0; j < sirina; j++) {
					if ((kodovi[i][j] >= 48 && kodovi[i][j] < 58)) {
						elementi[i*sirina + j] = new sf::RectangleShape;
						elementi[i*sirina + j]->setFillColor(boje[kodovi[i][j] - 48]);
						elementi[i*sirina + j]->setPosition((j+1/2.f)*sirinaEl, (i+1/2.f)*visinaEl);
						elementi[i*sirina + j]->setSize(sf::Vector2f(sirinaEl, visinaEl));
						elementi[i*sirina + j]->setOrigin(elementi[i*sirina + j]->getSize().x/2.f, elementi[i*sirina + j]->getSize().y/2.f);
						elementi[i*sirina + j]->setScale(0.92f, 0.92f);
						brojElemenata++;
					}
					else {
						elementi[i*sirina + j] = NULL;
					}
				}
			}
			//pravljenje dodatnik elemenata - lopta, udaraljka
			lopta = new KontrolaLopte(velicinaEkrana.x / 2.f, velicinaEkrana.y*3.f / 4.f, brzinaLopte);
			udaraljka.setPosition(velicinaEkrana.x / 2.f, velicinaEkrana.y*0.95f);
			udaraljka.setSize(sf::Vector2f(sirinaUdaraljke, 35));
			udaraljka.setOrigin(udaraljka.getSize().x / 2.f, 0);
			udaraljka.setFillColor(bojaUdaraljke);
		}
		
	}

	void crtaj(sf::RenderWindow &window, float deltaTime, float inputKorisnika) {
		sf::FloatRect okvirLopte = lopta->lopta.getGlobalBounds();
		for (int i = 0; i < sirina*visina; i++) {
			if (elementi[i] != NULL) {
				//provjerava se da li se lopta sudara s elementom
				if (okvirLopte.intersects(elementi[i]->getGlobalBounds())) {
					//ako se sudari mjenja se ugao odbijanja i brise se element
					lopta->odbiSe(elementi[i]->getPosition(), elementi[i]->getSize());
					delete elementi[i];
					elementi[i] = NULL;
					brojElemenata--;
					break;
				}
			}
		}
		//crtanje svih postojecih elemenata
		for (int i = 0; i < sirina*visina; i++) {
			if (elementi[i] != NULL) {
				window.draw(*elementi[i]);
			}
		}
		//kontrola udarajke
		udaraljka.move(800.f*inputKorisnika*deltaTime, 0);
		//provjera da ne izadje s lijeve strane
		if (udaraljka.getPosition().x < udaraljka.getSize().x / 2.f) {
			udaraljka.setPosition(udaraljka.getSize().x / 2.f, udaraljka.getPosition().y);
		}
		//isto za desnu stranu
		if (udaraljka.getPosition().x > velicinaEkrana.x - udaraljka.getSize().x / 2.f) {
			udaraljka.setPosition(velicinaEkrana.x - udaraljka.getSize().x / 2.f, udaraljka.getPosition().y);
		}
		//provjera odbijanja
		if (udaraljka.getPosition().y < lopta->lopta.getPosition().y + lopta->radius) {
			if ((abs(udaraljka.getPosition().x - lopta->lopta.getPosition().x) - 10.f) < udaraljka.getSize().x / 2.f)
				lopta->ugao = 180 - lopta->ugao + (rand() % 13 - 6) + inputKorisnika*(rand() % 8);
			else {
				izgubio = true;
				return;
			}
		}

		//cratanje lopte
		lopta->crtaj(window, deltaTime);

		//crtanje udaraljke
		window.draw(udaraljka);
	}
	
	int jelGotovo() {
		if (izgubio) return -1;
		if (brojElemenata == 0) return 1;
		return 0;
	}

	~KontrolaNivoa()
	{
		if (brojElemenata != 0) {
			for (int i = 0; i < visina*sirina; i++) {
				if (elementi[i] != NULL) {
					delete elementi[i];
				}
			}
		}
		delete[]elementi;
	}
};

