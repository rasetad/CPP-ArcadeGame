#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "KontrolaLopte.h"
#include "KontrolaNivoa.h"

using namespace std;

const sf::Vector2f velicinaEkrana(1280, 720);
const int ukupanBrojNivoa = 4;

int main() {
	sf::RenderWindow window(sf::VideoMode(velicinaEkrana.x, velicinaEkrana.y), "Igra odbijanja"/*, sf::Style::Fullscreen*/);
	window.setFramerateLimit(100);
	window.clear(sf::Color::Black);
	KontrolaNivoa *nivo = NULL;
	//ucitavanje fonta
	sf::Font font;
	font.loadFromFile("Resources\\arial.ttf");
	sf::Text text;
	text.setFillColor(sf::Color::White);
	text.setFont(font);
	text.setCharacterSize(50);
	text.setPosition(velicinaEkrana.x *3.f/ 7.f, velicinaEkrana.y / 2.f);

	//petlja nivoa
	bool izgubljeno = false;
	for (int trenutniNivo = 1; trenutniNivo <= ukupanBrojNivoa; trenutniNivo++) {
		bool igraTraje = false;
		if (!izgubljeno) {
			nivo = new KontrolaNivoa("Resources\\" + to_string(trenutniNivo) + ".nivo", velicinaEkrana);
			text.setString("NIVO   "+to_string(trenutniNivo));
			window.draw(text);
			window.display();
		}
		


		float inputKorisnika = 0.f;
		sf::Clock clock; //satic za racunanje proteklog vremena izmedju frame-ova
		while (window.isOpen()) {
			float deltaTime = clock.restart().asSeconds();//proteklo vrijeme od pprethodnog framea
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed) {
					window.close();
					exit(0);
				}
				if (event.type == sf::Event::KeyPressed) {
					switch (event.key.code)
					{
					case sf::Keyboard::Space:
						//kad se pritisne space igra pocinje
						if (izgubljeno == true) {
							window.close();
							exit(0);
						}
						else {
							igraTraje = true;
							window.clear(sf::Color::Black);
						}break;
					case sf::Keyboard::Left:
					case sf::Keyboard::A:
						inputKorisnika = -1.f;break;
					case sf::Keyboard::Right:
					case sf::Keyboard::D: //dutko
						inputKorisnika = 1.f;break;
					default:
						inputKorisnika = 0.f;break;
					}
				}
				else {
					inputKorisnika = 0.f;
				}
			}
			if (igraTraje) {
				// brisanje ekrana
				window.clear(sf::Color::Black);
				/* //kod za ostavljanje traga
				sf::RectangleShape ab;
				ab.setSize(sf::Vector2f(1280, 720));
				ab.setFillColor(sf::Color::Color(0, 0, 0, 5));
				window.draw(ab);*/

				// pozivaje crtanja
					nivo->crtaj(window, deltaTime, inputKorisnika);
					if (nivo->jelGotovo() != 0) {
						if (nivo->jelGotovo() == -1) {
							izgubljeno = true;
							text.setString("IZGUBILI STE");
							window.draw(text);
							window.display();
							break;
						}
						else {
							break;
						}
					}
				// prikazivanje framea
				window.display();
			}
		}
	}
	return 0;
}