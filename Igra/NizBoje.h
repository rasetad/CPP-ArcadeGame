#pragma once
#include <SFML\Graphics.hpp>
#include <assert.h>

class NizBoje
{
	sf::Color *niz;
	int duzina_niza;
public:

	NizBoje()
	{
		NizBoje(0);
	}
	NizBoje(int _duzina) {
		duzina_niza = _duzina;
		niz = new sf::Color[duzina_niza];
	}
	int duzina() { return duzina_niza; }

	void promjeniDuzinu(int nova_duzina) {
		sf::Color *novi_niz = new sf::Color[nova_duzina];
		int kopirani_elementi = (nova_duzina > duzina_niza) ? duzina_niza : nova_duzina;
		for (int indeks = 0; indeks < kopirani_elementi; indeks++)
			novi_niz[indeks] = niz[indeks];
		delete[] niz;
		niz = novi_niz;
		duzina_niza = nova_duzina;
	}

	sf::Color& operator[](int indeks)
	{
		assert(indeks >= 0 && indeks < duzina_niza);	// ako je indeks manji od nule i veci ili jednak od duzine niza onda je greska
		return niz[indeks];
	}

	~NizBoje()
	{
		delete[] niz;
	}
};

